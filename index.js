const axios = require('axios').default;

(async () => {
    // await new Promise(r => { setTimeout(r, 5000) })
    const db_name = process.env.DB_NAME || 'health_watcher'
    const api_url = process.env.API_URL || 'http://localhost:3001'
    let produceRandomMeasurements = async (context_id, sensor_id) => {
        console.log(`Sensor: ${context_id}`)
        console.log(`Context: ${sensor_id}`)
        await new Promise(r => setTimeout(r, 10000))
        while (true) {
            axios.post(`${api_url}/measurement/v3`, {
                context_id,
                device: {
                    id: sensor_id,
                    signal: Math.random()*90
                }
            })
            console.log(`[INFO] Registro enviado a las ${new Date()}`)
            await new Promise(r => setTimeout(r, 2000))
        }
    }

    let connection
    const r = require('rethinkdb')
    const db = require('./database')
    try {
        connection = await db.createConnection()

        let dbs = await r.dbList().run(connection)
        if (dbs.includes(db_name))
            throw new Error("DB already exists")

        await r.dbCreate(db_name).run(connection)
        await r.db(db_name).tableCreate('context').run(connection)
        await r.db(db_name).tableCreate('sensor').run(connection)
        await r.db(db_name).tableCreate('actuator').run(connection)
        await r.db(db_name).tableCreate('measurement').run(connection)

        await r.db(db_name).table('actuator').indexCreate("context").run(connection)
        await r.db(db_name).table('sensor').indexCreate("context").run(connection)
        await r.db(db_name).table('measurement').indexCreate("sensor").run(connection)

        let context = await r.db(db_name).table('context').insert({ name: "Consultorio 1", enabled: true }).run(connection)
        await r.db(db_name).table('actuator').insert({ context: context.generated_keys[0] }).run(connection)
        let sensor = await r.db(db_name).table('sensor').insert({ name: "Sensor", units: "Medida", context: context.generated_keys[0], enabled: true }).run(connection)
        produceRandomMeasurements(context.generated_keys[0], sensor.generated_keys[0])
    } catch (error) {
        if (/already exists/.test(error.message)) {
            console.warn('[WARNING] Base de datos ya creada')
            let context_id = await r.db(db_name).table('context').run(connection)
                .then(cursor => cursor.toArray())
                .then(array => (array[0].id))

            let sensor_id = await r.db(db_name).table('sensor').run(connection)
                .then(cursor => cursor.toArray())
                .then(array => (array[0].id))
            
            produceRandomMeasurements(context_id, sensor_id)
        } else {
            console.error(error)
        }
    } finally {
        if (connection) connection.close()
    }
})()


